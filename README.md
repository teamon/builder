# Builder base image

### Available versions
- `builder:1.0`

### Updating
Edit `Dockerfile` and update `version` label. 
New release will be built after pushing to master.