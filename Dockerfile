FROM alpine:3.6
LABEL version="1.0"

# install dependencies
RUN apk add --no-cache --update bash openssl openssh-client rsync git

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]